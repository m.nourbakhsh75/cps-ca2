#include <arduino.h>
#include <LiquidCrystal.h>
#include <AltSoftSerial.h>

#define DATA_SIZE 30
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
String readData;
AltSoftSerial altSerial;

//Global Var
char lightData[DATA_SIZE];
bool newLightData = false;
char THData[DATA_SIZE];
bool newTHData = false;
bool calcIL = false;
bool calcTHL = false;
char THDataCopy[DATA_SIZE];
char lightDataCopy[DATA_SIZE];

void receiveLightData()
{
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;

    if (altSerial.available() > 0)
    {
        rc = altSerial.read();
        if (recvInProgress == true)
        {
            if (rc != endMarker)
            {
                lightData[ndx] = rc;
                ndx++;
            }
            else
            {
                lightData[ndx] = '\0';
                recvInProgress = false;
                ndx = 0;
                newLightData = true;
            }
        }
        else if (rc == startMarker)
        {
            recvInProgress = true;
        }
    }
}

void printLightOnLCD(char* data)
{
    lcd.setCursor(0, 1);
    lcd.print("Light = ");
    lcd.setCursor(8, 1);
    lcd.print(data);
}

void receiveTHData()
{
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;

    if (Serial.available() > 0)
    {
        rc = Serial.read();
        if (recvInProgress == true)
        {
            if (rc != endMarker)
            {
                THData[ndx] = rc;
                ndx++;
            }
            else
            {
                THData[ndx] = '\0';
                recvInProgress = false;
                ndx = 0;
                newTHData = true;
            }
        }
        else if (rc == startMarker)
        {
            recvInProgress = true;
        }
    }
}

void printTHOnLCD(char *data)
{
    lcd.setCursor(0, 0);
    lcd.print("TH=");
    lcd.setCursor(3, 0);
    lcd.print(data);
}

void calcIrrigation(char* lData,char* thData){
    String SlData = (String) lData;
    int index;
    String hString,tString;
    for(int i=0;i< DATA_SIZE;i++){
        if(thData[i] != ','){
            hString += thData[i];
        }else if(thData[i] == ','){
            index = i;
            break;
        }
    }
    for(int i=index+1;i<index+7;i++)
        tString += thData[i];
    float light = SlData.toDouble();
    float temperature = tString.toDouble();
    float humidity = hString.toDouble();
    lcd.setCursor(-4, 2);
    lcd.print("Irrigation : ");
    lcd.setCursor(2, 3);
    if(humidity > 80.00){
        lcd.print("None  ");
    }else if(humidity < 50.00){
        lcd.print("15 CC/M");
    }else if(humidity > 50.00 && humidity < 80.00){
        if(temperature < 25.00 && light < 600.00){
            lcd.print("10 D/M");
        }else if(temperature < 25.00 && light > 600.00){
            lcd.print("5 D/M");
        }else if(temperature > 25.00){
            lcd.print("10 D/M");
        }
    }
}

void setup()
{
    lcd.begin(20,4);
    Serial.begin(9600);
    altSerial.begin(9600);
}


void loop()
{
    receiveTHData();
    if(newTHData){
        calcTHL = true;
        printTHOnLCD(THData);
        strlcpy((char*)THDataCopy,(char*)THData,512);
        newTHData = false;
        THData[0] = '\0';
    }
    receiveLightData();
    if (newLightData)
    {
        calcIL = true;
        printLightOnLCD(lightData);
        strlcpy((char*)lightDataCopy,(char*)lightData,512);
        newLightData = false;
        lightData[0] = '\0';
    }
    if(calcIL == true && calcTHL == true){
        calcIrrigation(lightDataCopy,THDataCopy);
        calcIL = false;
        calcTHL = false;
    }
}