#include "arduino.h"


float lux=0.00;
float ADC_value=0.0048828125;
float LDR_value;

void setup() {
    pinMode(A0, INPUT);
    Serial.begin(9600); 
}

void loop() {
    LDR_value=analogRead(A0);
    lux=(250.000000/(ADC_value*LDR_value))-50.000000;
    Serial.println("<" + String(lux) + ">");
    delay(500);
}

