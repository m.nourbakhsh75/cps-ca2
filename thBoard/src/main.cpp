#include "Arduino.h"
#include "Wire.h"

using namespace std;

// SHT25 I2C address is 0x40(64)
#define Addr 0x40

float humidity, cTemp;

String state = "requestHumidity"; // requestHumidity, readHumidity, requestTemp, readTemp, send

unsigned int data[2];

void readHumidity()
{
    if (state == "requestHumidity")
    {
        // Start I2C transmission
        Wire.beginTransmission(Addr);
        // Send humidity measurement command, NO HOLD master
        Wire.write(0xF5);
        // Stop I2C transmission
        Wire.endTransmission();

        state = "readHumidity";
    }
    else if (state == "readHumidity")
    {
        // Request 2 bytes of data
        Wire.requestFrom(Addr, 2);
        // Read 2 bytes of data
        // humidity msb, humidity lsb
        if (Wire.available() == 2)
        {
            data[0] = Wire.read();
            data[1] = Wire.read();
            // Convert the data
            humidity = (((data[0] * 256.0 + data[1]) * 125.0) / 65536.0) - 6;

            state = "requestTemp";
        }
    }
}

void readTemperature()
{
    if (state == "requestTemp")
    {
        // Start I2C transmission
        Wire.beginTransmission(Addr);
        // Send temperature measurement command, NO HOLD master
        Wire.write(0xF3);
        // Stop I2C transmission
        Wire.endTransmission();

        state = "readTemp";
    }
    else if (state == "readTemp")
    {
        // Request 2 bytes of data
        Wire.requestFrom(Addr, 2);
        // Read 2 bytes of data
        // temp msb, temp lsb
        if (Wire.available() == 2)
        {
            data[0] = Wire.read();
            data[1] = Wire.read();
            // Convert the data
            cTemp = (((data[0] * 256.0 + data[1]) * 175.72) / 65536.0) - 46.85;

            state = "send";
        }
    }
}

void sendData()
{
    if (state == "send")
    {
        Serial.println("<" + String(humidity) + "," + String(cTemp) + ">");
        state = "requestHumidity";
    }
}

void setup()
{
    // Initialise I2C communication as MASTER
    Wire.begin();
    // Initialise serial communication, set baud rate = 9600
    Serial.begin(9600);
    state = "requestHumidity";

    // delay(300);
}

void loop()
{
    readHumidity();
    readTemperature();
    sendData();
    delay(250);
}